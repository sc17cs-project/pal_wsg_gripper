^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package pal_wsg_gripper_controller_configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.0.6 (2018-04-19)
------------------

0.0.5 (2018-02-08)
------------------

0.0.4 (2018-02-07)
------------------
* remove 'gazebo' sufix
* Contributors: Jordi Pages

0.0.3 (2018-02-06)
------------------
* add controller configuration for finger sensors
* activate current limit controller
* Contributors: Jordi Pages

0.0.2 (2018-01-24)
------------------

0.0.1 (2018-01-24)
------------------
* Initial commit
* Contributors: Jordi Pages
