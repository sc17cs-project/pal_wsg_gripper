^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package pal_wsg_gripper_controller_configuration_gazebo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.0.6 (2018-04-19)
------------------

0.0.5 (2018-02-08)
------------------
* add missing install rule for config folder
* Contributors: Jordi Pages

0.0.4 (2018-02-07)
------------------

0.0.3 (2018-02-06)
------------------
* add fingertip force sensors
* Contributors: Jordi Pages

0.0.2 (2018-01-24)
------------------
* fix installation rule
* Contributors: Jordi Pages

0.0.1 (2018-01-24)
------------------
* Initial commit
* Contributors: Jordi Pages
